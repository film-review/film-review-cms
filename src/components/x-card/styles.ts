/**
 * @format
 */
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: 350,
  },
});

export default useStyles;
