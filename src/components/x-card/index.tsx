/**
 * @format
 */

import React, { FC } from 'react';
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
} from '@material-ui/core';
import useStyles from './styles';
import { Props } from './typed';

const XCard: FC<Props> = (props: Props) => {
  const { onClick, content } = props;
  const styles = useStyles();
  return (
    <Card className={styles.root}>
      <CardActionArea>
        <CardContent>
          <p>{content}</p>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={onClick}>
          Xem chi tiết
        </Button>
      </CardActions>
    </Card>
  );
};

export default XCard;
