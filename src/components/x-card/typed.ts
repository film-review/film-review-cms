/**
 * @format
 */

export declare type Props = {
  onClick: () => void;
  content: string;
};
