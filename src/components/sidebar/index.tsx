/**
 * @format
 */

import React, { FC } from 'react';
import useStyles from './styles';
import { NavLink } from 'react-router-dom';

const Sidebar: FC = () => {
  const styles = useStyles();
  return (
    <div className={styles.root}>
      <ul>
        <li>
          <NavLink to="/" exact>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/hello1" exact>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/hello2" exact>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/hello3" exact>
            Home
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Sidebar;
