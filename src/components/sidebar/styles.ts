/**
 * @format
 */
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    flex: 1,
    height: '100vh',
    backgroundColor: '#FFF',
  },
});

export default useStyles;
