/**
 * @format
 */

import { makeStyles } from '@material-ui/core/styles';
import { createStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuButton: {
      marginRight: theme.spacing(2),
    },
    brandLogo: {
      flex: 1,
    },
    brandLogoText: {
      color: '#FFF',
    },
  }),
);

export default useStyles;
