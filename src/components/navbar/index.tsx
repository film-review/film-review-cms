/**
 * format
 */

import React, { FC } from 'react';
import { AppBar, Button, IconButton, Toolbar, Typography } from '@material-ui/core';
import { Menu } from '@material-ui/icons';
import useStyles from './styles';
import { Link } from 'react-router-dom';

const NavBar: FC = () => {
  const styles = useStyles();
  return (
      <AppBar position={'static'} color={'primary'}>
        <Toolbar>
          <IconButton
            color={'inherit'}
            edge={'start'}
            className={styles.menuButton}>
            <Menu />
          </IconButton>
          <Typography className={styles.brandLogo}>
            <Link to="/" className={styles.brandLogoText}>Film Review Dashboard</Link>
          </Typography>
          <Button color={'inherit'}>Login</Button>
        </Toolbar>
      </AppBar>
  )
}

export default NavBar;
