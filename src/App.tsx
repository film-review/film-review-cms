/**
 * @format
 */

import React, { FC } from 'react';
import './App.css';
import NavBar from './components/navbar';
import Sidebar from './components/sidebar';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './routes/home';

const App: FC = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <div className={'app-style'}>
        <Sidebar />
        <Route path="/" exact component={Home} />
        <Route path="/hello1" exact component={() => <p>Hello1</p>} />
        <Route path="/hello2" exact component={() => <p>Hello2</p>} />
        <Route path="/hello3" exact component={() => <p>Hello3</p>} />
      </div>
    </BrowserRouter>
  );
};

export default App;
