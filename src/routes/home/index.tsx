/**
 * @format
 */

import React, { FC } from 'react';
import useStyles from './styles';
import XCard from '../../components/x-card';
import { Grid } from '@material-ui/core';

const Home: FC = () => {
  const styles = useStyles();
  return (
    <div className={styles.root}>
      <Grid container spacing={2}>
        <Grid item md={4}>
          <XCard content={'Hello'} onClick={() => alert('Hello')} />
        </Grid>
        <Grid item md={4}>
          <XCard content={'Hello 2'} onClick={() => alert('Hello')} />
        </Grid>
        <Grid item md={4}>
          <XCard content={'Hello 3'} onClick={() => alert('Hello')} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;
