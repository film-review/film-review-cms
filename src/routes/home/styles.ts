/**
 * @format
 */

import { makeStyles } from '@material-ui/core/styles';
import { createStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      flex: 6,
      backgroundColor: '#EFEFEF',
      padding: 32,
    },
  });
});

export default useStyles;
